/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.hegira_metamodel.utils;

import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TBinaryProtocol;

import it.polimi.hegira.hegira_metamodel.Metamodel;

public class ThriftSerializator {
	private TDeserializer deserializer;
	private TSerializer serializer;
	
	public ThriftSerializator() {
		deserializer = new TDeserializer(new TBinaryProtocol.Factory());
		serializer = new TSerializer(new TBinaryProtocol.Factory());
	}

	public byte[] serialize(Metamodel message) {
		try {
			return serializer.serialize(message);
		} catch (TException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Metamodel deserialize(byte[] binary) {
		Metamodel metamodel = null; 
		try {
			metamodel = new Metamodel();
			deserializer.deserialize(metamodel, binary);
		} catch (TException e) {
			e.printStackTrace();
			return null;
		}
		return metamodel;
	}

}
