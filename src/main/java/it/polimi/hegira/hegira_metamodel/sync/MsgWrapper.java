/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.hegira_metamodel.sync;

import java.io.Serializable;

/**
 * @author Marco Scavuzzo
 *
 */
public class MsgWrapper implements Serializable{
	private static final long serialVersionUID = -5980347146711158646L;
	private String opId;
	private Operations operation;
	//should contain Thrift serialized metamodel
	private byte[] content;
	private String instanceId;
	
	public MsgWrapper(){
		
	}
	
	public MsgWrapper(String opId, Operations operation) {
		this.opId = opId;
		this.operation = operation;
	}

	/**
	 * @return the key
	 */
	public String getOpId() {
		return opId;
	}

	/**
	 * @param key the key to set
	 */
	public void setOpId(String opId) {
		this.opId = opId;
	}

	/**
	 * @return the operation
	 */
	public Operations getOperation() {
		return operation;
	}

	/**
	 * @param operation the operation to set
	 */
	public void setOperation(Operations operation) {
		this.operation = operation;
	}

	/**
	 * @return the content
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
}
